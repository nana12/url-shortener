<?php


namespace App\System;

/** SINGLETON USING PDO TO CONNECT TO DB */

class DB
{
    private static $instance;


    private function __construct()
    {
        $driver = config('database.driver');
        $host = config('database.host');
        $user = config('database.username');
        $pass = config('database.password');
        $dbName = config('database.dbname');

        $dsn = "$driver:host=$host;dbname=$dbName";

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ];

        self::$instance = new \PDO($dsn, $user, $pass, $options);
    }

    public static function getInstance()
    {
        if (self::$instance === NULL) {
            new self();
        }

        return self::$instance;
    }


    private function __clone(){}

    private function __wakeup(){}

}