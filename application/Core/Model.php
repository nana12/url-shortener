<?php

namespace App\Core;

use App\System\DB;

class Model
{
    protected $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    public function transaction(callable $call)
    {
        $this->db->beginTransaction();

        $call();

        return $this->db->commit();
    }


}